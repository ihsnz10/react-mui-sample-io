// material-ui
import { useTheme } from '@mui/material/styles';
import logo from 'assets/images/video-logo.png';

/**
 * if you want to use image instead of <svg> uncomment following.
 *
 * import logoDark from 'assets/images/logo-dark.svg';
 * import logo from 'assets/images/logo.svg';
 *
 */

// ==============================|| LOGO SVG ||============================== //

const Logo = () => {
  const theme = useTheme();

  return (
    <div style={logoStyle}>
      <img src={logo} alt="AnyVideo" height="36" />
      <h2 style={textStyle}>AnyVideo</h2>
    </div>
  );
};

export default Logo;

const logoStyle = {
  display: 'flex',
  alignItems: 'center'
};

const textStyle = {
  marginLeft: '16px'
}